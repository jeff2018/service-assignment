import { Injectable } from '@angular/core';

@Injectable()
export class CounterService {
  activeToInactiveCounter = 0;
  inactiveToActiveCounter = 0;

  constructor() { }

  incrementActiveToInactive() {
    this.activeToInactiveCounter++;
    console.log('Active to Inactive ' + this.activeToInactiveCounter);
    return this.activeToInactiveCounter;
  }

  incrementInactiveToActive() {
    this.inactiveToActiveCounter++;
    console.log('Inactive to Active ' + this.inactiveToActiveCounter);
    return this.inactiveToActiveCounter;
  }

  ATI() {
    return this.activeToInactiveCounter;
  }

  ITA() {
    return this.inactiveToActiveCounter;
  }
}

import { Component, OnInit } from '@angular/core';
import { CounterService } from '../counter.service';

@Component({
  selector: 'app-count',
  templateUrl: './count.component.html',
  styleUrls: ['./count.component.css']
})
export class CountComponent implements OnInit {
  countInactive: number;
  countActive: number;

  constructor(private countServices: CounterService) {

  }

  ngOnInit() {
    this.countInactive = this.countServices.ITA();
    this.countActive = this.countServices.ATI();
  }

  onClick() {
    this.countInactive = this.countServices.ITA();
    this.countActive = this.countServices.ATI();
  }
}
